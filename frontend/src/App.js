import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Home from './components/pages/Home/index'; 
import './index.css';
import Sample from './components/pages/Sample';
import Users from './components/pages/Users';
import Projects from './components/pages/Projects';
import Navbar from './components/layout/Navbar';
import Sidebar from './components/layout/Sidebar';
import Footer from './components/layout/Footer';
import Login from './components/pages/Login';
import Logout from './components/pages/Logout';
import Tags from './components/pages/Tag ';
import Task from './components/pages/Task';
import Comments from  './components/comments/comment';

const isAuthenticated = () => {
  return !!localStorage.getItem('authToken');
};

const PrivateRoute = ({ element }) => {
  return isAuthenticated() ? element : <Navigate to="/login" />;
};

function App() {
  return (
    <Router>
      <Navbar />
      <Sidebar />
      <Routes>
        <Route path="/login" element={<Login />}/>
        <Route path="/" element={<PrivateRoute element={<Home />} />}/>
        <Route path="/sample" element={<PrivateRoute element={<Sample />} />}/>
        <Route path="/users" element={<PrivateRoute element={<Users />} />}/>
        <Route path="/logout" element={<PrivateRoute element={<Logout />} />}/>
        <Route path="/projects" element={<PrivateRoute element={<Projects />} />}/>
        <Route path="/tag" element={<PrivateRoute element={<Tags />} />}/>
        <Route path="/task" element={<PrivateRoute element={<Task />} />}/>
        <Route path="/comments" element={<PrivateRoute element={<Comments />} />}/>
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
