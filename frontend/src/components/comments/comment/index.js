import { useState, useEffect } from 'react';
import { FaCirclePlus } from "react-icons/fa6";
import Container from '../../layout/Container';
import Table from '../../layout/Table';
import style from './Comments.module.css';
import Modal from '../../layout/Modal';
import CommentForm from './CommentForm';

function Comments({ taskId }) {
    const [headers, setHeaders] = useState([]);
    const [items, setItems] = useState([]);
    const [modalShow, setModalShow] = useState(false);
    const [currentComment, setCurrentComment] = useState({ content: '', idUser: '' });

    useEffect(() => {
        fetchComments();
    }, []);

    const fetchComments = async () => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/comment?taskId=${taskId}`);
            const data = await response.json();
            if (data.data && data.data.length) {
                setHeaders(Object.keys(data.data[0]));
                setItems(data.data);
            }
        } catch (error) {
            console.error('Erro ao buscar dados:', error);
        }
    };

    const handleAddComment = async (comment) => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/comment`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ ...comment, idTask: taskId })
            });
            const data = await response.json();
            if (data.type === 'success') {
                fetchComments();
                setModalShow(false);
            } else {
                console.error('Erro ao adicionar comentário:', data.message);
            }
        } catch (error) {
            console.error('Erro ao enviar dados:', error);
        }
    };

    const handleEditComment = async (comment) => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/comment/${comment.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(comment)
            });
            const data = await response.json();
            if (data.type === 'success') {
                fetchComments();
                setModalShow(false);
            } else {
                console.error('Erro ao editar comentário:', data.message);
            }
        } catch (error) {
            console.error('Erro ao enviar dados:', error);
        }
    };

    const handleDeleteComment = async (id) => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/comment/${id}`, {
                method: 'DELETE'
            });
            const data = await response.json();
            if (data.type === 'success') {
                fetchComments();
            } else {
                console.error('Erro ao deletar comentário:', data.message);
            }
        } catch (error) {
            console.error('Erro ao deletar dados:', error);
        }
    };

    const handleFormSubmit = (comment) => {
        if (comment.id) {
            handleEditComment(comment);
        } else {
            handleAddComment(comment);
        }
    };

    return (
        <Container className={`${style.pageComments} full-screen`}>
            <div className={style.buttonActions}>
                <button className='btn btn-success' onClick={() => { setModalShow(true); setCurrentComment({ content: '', idUser: '' }); }}>
                    <FaCirclePlus />
                    Adicionar
                </button>
            </div>
            {items.length >= 1 ? <Table headers={headers} items={items} onDelete={handleDeleteComment} onEdit={(item) => { setModalShow(true); setCurrentComment(item); }} /> : ''}
            <Modal
                show={modalShow}
                onClose={() => setModalShow(false)}
                children={<CommentForm comment={currentComment} onSubmit={handleFormSubmit} />}
                setStateShow={() => { setModalShow(!modalShow); }}
            />
        </Container>
    );
}

export default Comments;
