import React, { useState, useEffect } from 'react';

function CommentForm({ comment, onSubmit }) {
    const [content, setContent] = useState(comment.content);
    const [idUser, setIdUser] = useState(comment.idUser);

    useEffect(() => {
        setContent(comment.content);
        setIdUser(comment.idUser);
    }, [comment]);

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit({ ...comment, content, idUser });
    };

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="content">Comentário:</label>
                <input
                    type="text"
                    id="content"
                    value={content}
                    onChange={(e) => setContent(e.target.value)}
                />
            </div>
            <div>
                <label htmlFor="idUser">ID Usuário:</label>
                <input
                    type="number"
                    id="idUser"
                    value={idUser}
                    onChange={(e) => setIdUser(e.target.value)}
                />
            </div>
            <button className="btn btn-info" type="submit">Enviar</button>
        </form>
    );
}

export default CommentForm;
