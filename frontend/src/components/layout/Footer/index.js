import style from './Footer.module.css'
import Container from "../Container";

function Footer(props) {
    return (
        <div className={`${style.footer} ${props.footerClass}`}>
            <Container>
                <div className='text-center'>
                    &copy;Desenvolvimento Web, 2024
                </div>
            </Container>
        </div>
    )
}

export default Footer;