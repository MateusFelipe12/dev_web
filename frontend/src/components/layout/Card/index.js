import { FaCss3Alt, FaHtml5, FaJs, FaReact } from 'react-icons/fa6';
import { BsArrowRight } from 'react-icons/bs';
import styles from './Card.module.css'

function Card({ titulo, descricao }) {
    return (
        <section className={styles.card}>
            <h3>{titulo}</h3>
            <p>{descricao}</p>
            <div className={styles.card_footer}>
                <div className={styles.card_icones}>
                    <FaHtml5 />
                    <FaCss3Alt />
                    <FaJs />
                    <FaReact />
                </div>
                <button className={styles.botao}>
                    <BsArrowRight/>
                </button>
            </div>
        </section>
    )
}

export default Card;



