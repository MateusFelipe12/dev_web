import { Link } from "react-router-dom";
import style from './Navbar.module.css'
import Container from "../Container";

function Navbar(props) {
    return (
        <div className={`${style.navbar}`}>
            <Container>
                <div className={style.header}>
                    <Link to='/'>DevWeb</Link>
                </div>
            </Container>
        </div>
    )
}

export default Navbar;