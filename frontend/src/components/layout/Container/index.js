import style from './Container.module.css';

function Container(props) {
    return (<div className={`${style.container} ${props.className}`}>{props.children}</div>)
}

export default Container;