import { FaHome, FaWindowClose, FaUser, FaComment } from 'react-icons/fa';
import { Link } from "react-router-dom";
import style from './Sidebar.module.css';
import { PiPencil } from 'react-icons/pi';

function Sidebar(props) {
    return (
        <div className={`${style.sidebar} full-screen`}>
            <div className={style.menu}>
                <Link to='/'>
                    <div className={style.icon}><FaHome /></div>
                    <div className={style.text}>Home</div>
                </Link>
                <Link to='/projects'>
                    <div className={style.icon}><FaWindowClose /></div>
                    <div className={style.text}>Projetos</div>
                </Link>
                <Link to='/users'>
                    <div className={style.icon}><FaUser /></div>
                    <div className={style.text}>Usuários</div>
                </Link>
                <Link to='/task'>
                    <div className={style.icon}><FaUser /></div>
                    <div className={style.text}>Tarefas</div>
                </Link>
                <Link to='/tag'>
                    <div className={style.icon}><PiPencil /></div>
                    <div className={style.text}>Etiquetas</div>
                </Link>
                <Link to='/comments'>
                    <div className={style.icon}><FaComment /></div>
                    <div className={style.text}>Comentários</div>
                </Link>
            </div>
        </div>
    );
}

export default Sidebar;
