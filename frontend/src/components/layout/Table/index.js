import React from 'react';
import PropTypes from 'prop-types';
import style from './Table.module.css';

const Table = ({ headers, items, onDoubleClickRow }) => {
  return (
    <div className={style.tableResponsive}>
      <table className={style.table}>
        <thead>
          <tr>
            {headers.map((header, index) => (
              <th key={index}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {items.map((item, rowIndex) => (
            <tr key={rowIndex} onDoubleClick={onDoubleClickRow}>
              {Object.keys(item).map((cell, cellIndex) => (
                <td key={cellIndex} header={cell}>
                  {typeof item[cell] === 'string' && /<\/?[a-z][\s\S]*>/i.test(item[cell]) ? (
                    <div dangerouslySetInnerHTML={{ __html: item[cell] }} />
                  ) : (
                    item[cell]
                  )}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

Table.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string).isRequired,
  items: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.node])
    )
  ).isRequired
};

export default Table;
