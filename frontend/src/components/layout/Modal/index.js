import style from './Modal.module.css';

const Modal = ({ show, onClose, onOpen, children}) => {
    if (!show) {
        return null;
    }

    if (typeof onOpen == 'function') onOpen();

    return (
        <div className={style.modalOverlay}>
            <div className={style.modal}>
                <button className={style.modalClose} onClick={onClose}>
                    &times;
                </button>
                <div className={style.modalContent} >{children}</div>
            </div>
        </div>
    );
};

export default Modal;
