import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Container from '../../layout/Container';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();
  localStorage.removeItem('authToken');

  return (
    <Container className="page-home full-screen">
      <div>
        <h1>Você fez logout</h1>
        <Link to='/login'>Login</Link>
      </div>
    </Container>
  );
};

export default Login;
