import { useState } from 'react'
import { FaCirclePlus } from "react-icons/fa6";
import Container from '../../layout/Container';
import Table from '../../layout/Table';
import style from './Users.module.css';
import Modal from '../../layout/Modal';

function Users() {
    const [headers, setHeaders] = useState([]);
    const [items, setItems] = useState([]);
    const [modalShow, setmodalShow] = useState(false);
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [showEdition, setShowEdition] = useState(false);


    function formCreate() {
        return <form onSubmit={showEdition ? update : create}>
            <div>
                <label htmlFor="name">Nome:</label>
                <input
                    type="text"
                    id="name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
            </div>
            <div>
                <label htmlFor="email">E-mail:</label>
                <input
                    type="email"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    disabled={showEdition}
                />
            </div>
            <div>
                <label htmlFor="password">Senha:</label>
                <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
            </div>
            <button className="btn btn-info" type="submit">Enviar</button>
        </form>
    }

    function create(e) {
        e?.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/user`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                password,
                email
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.data && typeof data.data === 'number') {
                    fetch(`${process.env.REACT_APP_API_URL}/user/${data.data}`)
                        .then(response => response.json())
                        .then(data => {
                            const user = data.data;
                            if (user && user.email) {
                                setItems([...items, user])
                                setHeaders(Object.keys(user))
                            }
                            return false;
                        })
                        .catch(error => console.error('Erro ao buscar dados:', error));
                }
                setPassword('');
                setEmail('');
                setName('');
                setmodalShow(false)
            })
            .catch(error => console.error('Erro ao enviar dados:', error));
    }

    function update(e) {
        e?.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/user/${email}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                password,
                email
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.data) {
                    getAll();
                }
                setPassword('');
                setEmail('');
                setName('');
                setmodalShow(false)
            })
            .catch(error => console.error('Erro ao enviar dados:', error));
    }

    function editUser(e) {
        const tdEmail = e.target.parentElement.querySelector('[header="email"]')
        if (tdEmail) {
            const email = tdEmail.textContent?.trim();
            if (email && email.length > 0) {
                fetch(`${process.env.REACT_APP_API_URL}/user/email/${email}`)
                    .then(response => response.json())
                    .then(data => {
                        const user = data.data;
                        if (user) {
                            setEmail(user.email);
                            setName(user.name);
                            setPassword('');
                            setShowEdition(true);
                            setmodalShow(true);
                        }
                    })
                    .catch(error => console.error('Erro ao enviar dados:', error));
            }
        }
    }

    function toggleModal() {
        if (showEdition) {
            update();
        } else {
            create();
            setmodalShow(!modalShow);
        }
    }

    function getAll() {
        fetch(`${process.env.REACT_APP_API_URL}/user`)
            .then(response => response.json())
            .then(data => {
                const users = data.data;

                if (users.length) {
                    setHeaders(Object.keys(users[0]))
                    setItems(users)
                }
                return false;
            })
            .catch(error => console.error('Erro ao buscar dados:', error));
        return [];
    }

    return (
        <Container className={`${style.pageUsers} full-screen`} >
            <div className={style.buttonActions}>
                <button className='btn btn-success' onClick={() => { setmodalShow(true); }}>
                    <FaCirclePlus />
                    Adicionar
                </button>
            </div>
            {
                items.length >= 1 ?
                    <Table
                        headers={headers}
                        items={items}
                        onDoubleClickRow={editUser}
                    />
                    : 'Nenhum usuário encontrado' && getAll()}
            <Modal
                show={modalShow}
                onClose={toggleModal}
                children={formCreate()}
                setStateShow={() => { setmodalShow(!modalShow) }}
            />
        </Container>
    )
}

export default Users;
