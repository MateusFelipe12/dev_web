import { useState, useEffect } from 'react'
import { FaCirclePlus } from "react-icons/fa6";
import Container from '../../layout/Container';
import Table from '../../layout/Table';
import style from './Tags.module.css';
import Modal from '../../layout/Modal';

function Tags() {
    const [headers, setHeaders] = useState([]);
    const [items, setItems] = useState([]);
    const [modalShow, setModalShow] = useState(false);
    const [name, setName] = useState('');
    const [color, setColor] = useState('');

    function formCreate() {
        return (
            <form onSubmit={create}>
                <div>
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="color">Color:</label>
                    <input
                        type="color"
                        id="color"
                        value={color}
                        onChange={(e) => setColor(e.target.value)}
                    />
                </div>
                <button className="btn btn-info" type="submit">Submit</button>
            </form>
        );
    }

    function create(e) {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/tag`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                color,
            })
        })
        .then(response => response.json())
        .then(data => {
            if (data.data && typeof data.data === 'number') {
                fetch(`${process.env.REACT_APP_API_URL}/tag/${data.data}`)
                .then(response => response.json())
                .then(data => {
                    const tag = data.data;
                    if (tag && tag.id && typeof tag.id === 'number') {
                        setItems([...items, tag]);
                        setHeaders(Object.keys(tag));
                    }
                })
                .catch(error => console.error('Error fetching data:', error));
            }
        })
        .catch(error => console.error('Error sending data:', error))
        .finally(() => {
            setName("");
            setColor('');
            setModalShow(false);
        });
    }

    function toggleModal() {
        setModalShow(!modalShow);
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/tag`)
            .then(response => response.json())
            .then(data => {
                const tags = data.data;
                if (Array.isArray(tags) && tags.length) {
                    setHeaders(Object.keys(tags[0]));
                    setItems(tags);
                }
            })
            .catch(error => console.error('Error fetching data:', error));
    }, []);

    return (
        <Container className={`${style.pageTags} full-screen`}>
            <div className={style.buttonActions}>
                <button className='btn btn-success' onClick={toggleModal}>
                    <FaCirclePlus />
                    Add
                </button>
            </div>
            {items.length >= 1 ? <Table headers={headers} items={items} /> : ''}
            <Modal
                show={modalShow}
                onClose={toggleModal}
                children={formCreate()}
                setStateShow={() => { setModalShow(!modalShow) }}
            />
        </Container>
    );
}

export default Tags;
