import style from "./Home.module.css";
import { HiEmojiHappy } from "react-icons/hi";
import Container from '../../layout/Container';
function Home() {
    return (
        <Container className={`${style.pageHome} full-screen`}>
            <div>
                Bem vindo a pagina inicial! <HiEmojiHappy />            
            </div>
        </Container>
    )

}
export default Home;
