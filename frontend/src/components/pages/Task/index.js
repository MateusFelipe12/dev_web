import { useState, useEffect } from 'react';
import Container from '../../layout/Container';
import './Task.module.css'; // Importar o arquivo CSS para estilizar

function App() {
  const [tasks, setTasks] = useState([]);
  const [taskTitle, setTaskTitle] = useState('');
  const [taskDescription, setTaskDescription] = useState('');
  const [taskStatus, setTaskStatus] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [currentTaskIndex, setCurrentTaskIndex] = useState(null);

  useEffect(() => {
    fetchTasks();
  }, []);

  const fetchTasks = async () => {
    try {
      fetch(`${process.env.REACT_APP_API_URL}/task`)
      .then((response) => response.json())
      .then( (data) => {
        console.log(data)
        setTasks([...data.data]);
      })
    } catch (error) {
      console.error('Error fetching task:', error);
    }
  };

  const addTask = () => {
    if (taskTitle.trim() === '' || taskDescription.trim() === '') return;
    
    const newTask = {
      title: taskTitle,
      description: taskDescription,
      status: taskStatus
    };
  
    fetch(`${process.env.REACT_APP_API_URL}/task`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newTask)
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      return response.json();
    })
    .then(data => {
      setTasks([...tasks, data.data]);
      setTaskTitle('');
      setTaskDescription('');
    })
    .catch(error => {
      console.error('Error adding task:', error);
    });
  };
  

  const editTask = (index) => {
    const task = tasks[index];
    setTaskTitle(task.title);
    setTaskDescription(task.description);
    setIsEditing(true);
    setCurrentTaskIndex(index);
  };

  const updateTask = async () => {
    if (taskTitle.trim() === '' || taskDescription.trim() === '') return;
    const updatedTask = {
      title: taskTitle,
      description: taskDescription,
      status: taskStatus
    };
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/task/${tasks[currentTaskIndex].id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedTask)
      });
      const data = await response.json();
      const updatedTasks = tasks.map((task, index) =>
        index === currentTaskIndex ? data : task
      );
      setTasks(updatedTasks);
      setTaskTitle('');
      setTaskDescription('');
      setIsEditing(false);
      setCurrentTaskIndex(null);
    } catch (error) {
      console.error('Error updating task:', error);
    }
  };

  const deleteTask = async (index) => {
    if (window.confirm('Are you sure you want to delete this task?')) {
      try {
        await fetch(`http://localhost:3001/task/${tasks[index].id}`, {
          method: 'DELETE'
        });
        const newTasks = tasks.filter((_, i) => i !== index);
        setTasks(newTasks);
      } catch (error) {
        console.error('Error deleting task:', error);
      }
    }
  };

  const toggleTaskStatus = async (index) => {
    const updatedTask = {
      ...tasks[index],
      status: tasks[index].status === 'incomplete' ? 'complete' : 'incomplete'
    };
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/task/${tasks[index].id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedTask)
      });
      const data = await response.json();
      const updatedTasks = tasks.map((task, i) => (i === index ? data : task));
      setTasks(updatedTasks);
    } catch (error) {
      console.error('Error toggling task status:', error);
    }
  };

  return (
    <Container>
      <h1>Task List</h1>
      <div className="task-input">
        <input
          type="text"
          value={taskTitle}
          onChange={(e) => setTaskTitle(e.target.value)}
          placeholder="Add a new task title..."
          className="task-title-input"
        />
        <textarea
          value={taskDescription}
          onChange={(e) => setTaskDescription(e.target.value)}
          placeholder="Add a new task description..."
          className="task-description-input"
        />
        <select name="status" value={taskStatus} onChange={(e) => setTaskStatus(e.target.value)}>
          <option value="" disabled>Selecione um status</option>
          <option value="pending">Pendente</option>
          <option value="completed">Completo</option>
        </select>


        <button onClick={isEditing ? updateTask : addTask} className="add-task-button">
          {isEditing ? 'Update Task' : 'Add Task'}
        </button>
      </div>
      <ul className="task-list">
        {Object.keys(tasks).map((task, index) => (
          <li key={index} className={`task-item ${tasks[task].status}`}>
            <div className="task-header">
              <h3>{tasks[task].title}</h3>
              <div className="task-actions">
                <button onClick={() => editTask(index)} className="edit-button">Edit</button>
                <button onClick={() => deleteTask(index)} className="delete-button">Delete</button>
              </div>
            </div>
            <p className="task-description">{tasks[task].description}</p>
          </li>
        ))}
      </ul>
    </Container>
  );
}

export default App;
