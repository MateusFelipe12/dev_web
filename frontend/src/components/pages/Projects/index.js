import { useEffect, useState } from "react";
import Card from "../../layout/Card";
import styles from './Projects.module.css';
import Container from '../../layout/Container';
import Modal from '../../layout/Modal';
import { FaCirclePlus } from "react-icons/fa6";

function Projects() {

    const [projetos, setProjetos] = useState([])
    const [titulo, setTitulo] = useState('');
    const [descricao, setDescricao] = useState('');
    const [modalShow, setmodalShow] = useState(false);

    function formCreate() {
        return <form onSubmit={create}>
            <div>
                <label htmlFor="name">Título:</label>
                <input
                    type="text"
                    id="titulo"
                    value={titulo}
                    onChange={(e) => setTitulo(e.target.value)}
                />
            </div>
            <div>
                <label htmlFor="descricao">Descrição:</label>
                <input
                    type="descricao"
                    id="descricao"
                    value={descricao}
                    onChange={(e) => setDescricao(e.target.value)}
                />
            </div>
            <button className="btn btn-info" type="submit">Enviar</button>
        </form>
    }

    function create(e) {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/projeto`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                titulo,
                descricao
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data.data)
            setProjetos([...projetos, data.data])
            // setHeaders(Object.keys(user))
            setmodalShow(false)
        })
        .catch(error => console.error('Erro ao enviar dados:', error));
    }

    function toggleModal() {
        create()
        setmodalShow(!modalShow)
    }

    useEffect(() => {

        const buscarProjetos = async () => {
            fetch(`${process.env.REACT_APP_API_URL}/projeto`)
            .then(response => response.json())
            .then(data  =>  {
                setProjetos(data.data)
            })
            .catch(error => console.error(error))
        }

        buscarProjetos()
            
    }, []);

    return (
        <Container className={`full-screen`} >
            <div className={styles.buttonActions}>
                <button className='btn btn-success' onClick={() => { setmodalShow(true); }}>
                    <FaCirclePlus />
                    Adicionar
                </button>
            </div>
            <section className={styles.projetos}>
            {
                projetos.length > 0 ? (
                    <section className={styles.lista}> 
                        {
                            projetos.map(projeto => (
                                <Card 
                                key={projeto.id} 
                                titulo={projeto.titulo} 
                                descricao={projeto.descricao} />
                            ))
                        }
                    </section>
                ) : (
                    <p>Carregando...</p>
                )
            }
            </section>
            <Modal
                show={modalShow}
                onClose={toggleModal}
                children={formCreate()}
                setStateShow={() => { setmodalShow(!modalShow) }}
            />
        </Container>
    );
}

export default Projects;