const jwt = require('jsonwebtoken');
const authMiddleware = require('./middlewares/auth');
const session = require('express-session');
const bodyParser = require('body-parser');

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use(session({
    secret: '0cdb7bd15deb5f2034e1ea3dd4264d13',
    resave: false,
    saveUninitialized: true
}));

server.post('/login', async (req, res) => {
    const { email, password } = req.body;
    const userModel = require('./models/user');

    try {
        const user = await userModel.findOne({ where: { email } });

        if (user && password === user.password) {
            const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, { expiresIn: '1h' });
            res.json({ token });
        } else {
            res.status(401).send({
                type: 'error',
                message: 'Credenciais inválidas.'
            });
        }
    } catch (error) {
        res.status(500).send({
            type: 'error',
            message: 'Erro ao processar o login.'
        });
    }
});

server.post('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return res.status(500).send('Erro ao fazer logout.');
        }
        res.send('Logout realizado com sucesso!');
    });
});
