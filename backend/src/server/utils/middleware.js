const jwt = require('jsonwebtoken');


const checkTokenMiddleware = (req, res, next) => {
    try {
        const authHeader = req.headers['authorization'];
        if (!authHeader) {
            return res.status(401).send({
                type: 'error',
                message: 'Authorization header is missing'
            });
        }

        const token = authHeader.split(' ')[1]; // 'Bearer <token>'

        if (!token) {
            return res.status(401).send({
                type: 'error',
                message: 'Access token is missing'
            });
        }

        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return res.status(403).send({
                    type: 'error',
                    message: 'Invalid token'
                });
            }

            req.user = decoded;
            next();
        });
    } catch (error) {
        console.log(error)
        return res.status(500).send({
            type: 'error',
            message: 'Failed to authenticate token'
        });
    }
};

module.exports = {checkTokenMiddleware}