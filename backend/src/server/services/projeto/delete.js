const Projeto = require("../../models/projeto");

module.exports = async (req, res) => {
    try {
        let { id } = req.body;

        if (!id) {
            return res.status(200).send({
                type: 'warning',
                message: 'Ops! retorne um id valido !',
                data: []
            });
        }

        const response = await Projeto.findOne({
            where: {
                id: id
            }
        });

        if (!response) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado o registro!',
            data: []
            });
        }

        await response.destroy();
    
        return res.status(200).send({
            type: 'success',
            message: 'registro excluido com sucesso!',
            data: response
        });    
    } catch (error) {
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao atualizar o registro!',
            data: []
        });
    }
}