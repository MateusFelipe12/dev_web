const Projeto = require("../../models/projeto");

module.exports = async (req, res) => {
    try {
        let { id } = req.params;
        const updatedData = req.body;
        const response = await Projeto.update(updatedData, {
            where: {
                id: id
            }
        });

        if (!response) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado o registro!',
            data: []
            });
        }
    
        return res.status(200).send({
            type: 'success',
            message: 'Registro atualizado com sucesso!',
            data: response
        });    
    } catch (error) {
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao atualizar o registro!',
            data: []
        });
    }
}