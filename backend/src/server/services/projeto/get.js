const Projeto = require("../../models/projeto");

module.exports = async (req, res) => {
    try {
        const response = await Projeto.findAll();
        if (!response.length) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado os registro!',
            data: []
            });
        }
        return res.status(200).send({
            type: 'success',
            message: 'Registros encontrados com sucesso!',
            data: response
        });
    } catch (error) {
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao buscar os registros!',
            data: []
        });
    }
}