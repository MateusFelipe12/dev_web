const Projeto = require("../../models/projeto");

module.exports =  async (req, res) => {
  try {
    let { titulo, descricao} = req.body;

    if (!descricao || !titulo) {
      return res.send({
        type: 'error',
        message: `É necessario infrmar todos os campos para adicionar o registro`
      });
    }

    if (typeof descricao === 'string' && typeof titulo === 'string') {

        const projetoExists = await Projeto.findOne({ where: { titulo } });
        
        if (!projetoExists) {
            let response = await Projeto.create({ titulo, descricao });

            return res.status(200).send({
                type: 'success',
                message: 'Registro criado com sucesso',
                data: response
            });
        } else {
            return res.status(400).send({
                type: 'error',
                message: 'Erro ao criar registro, projeto já existe'
            });
        }
    } else {
        return res.status(400).send({
            type: 'error',
            message: 'Erro ao criar registro, tipo de dado inválido'
        });
    }
  } catch (error) {
    return res.send({
      type: 'error',
      message: error.message
    });
  }
}
