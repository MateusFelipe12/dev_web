const Projeto = require("../../models/projeto");

module.exports = async (req, res) => {
    try {
        let { id } = req.params;

        const response = await Projeto.findOne({
            where: {
                id: id
            }
        });

        if (!response) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado o registro!',
            data: []
            });
        }
    
        return res.status(200).send({
            type: 'success',
            message: 'Registro encontrado com sucesso!',
            data: response
        });    
    } catch (error) {
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao buscar os registro por Id!',
            data: []
        });
    }
}