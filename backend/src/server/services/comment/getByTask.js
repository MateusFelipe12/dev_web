const comment = require("../../models/comment");
const user = require("../../models/user");

module.exports =  async (req, res) => {
  try {
    const idCrude = Number(req.params.idTask);
    const idTask  = idCrude !== NaN ? idCrude : false;
    
    if (idTask && typeof idTask === 'number') {
      const comments = await comment.findAll({
        where: { idTask },
        include: [
          {
            model: user,
            attributes: ['name'],
          },
        ]
      });
      console.log(JSON.stringify(comments, null, 2));
      return res.status(200).send({
        type: 'success',
        data: comments
      })
      
    }
    return res.status(400).send({
      type: 'error',
      message: 'Registro não encontrado.'
    }); 
  } catch (error) {
    return res.status(400).send({
      type: 'error',
      message: error.message
    });
  }
}
