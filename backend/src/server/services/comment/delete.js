const comment = require("../../models/comment");

module.exports =  async (req, res) => {
  try {
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    
    if (id && typeof id === 'number') {
      const commentExists = await comment.findOne({ where: { id } });
      
      if(!!commentExists) {
        let response = await commentExists.destroy();
        if ( response !== null ) {
          return res.status(204).send();
        }
      }
    }
    return res.status(400).send({
      type: 'error',
      message: 'Não foi possivel deletar o registro.',
    });
  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
