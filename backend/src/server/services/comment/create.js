const comment = require("../../models/comment");
const task = require("../../models/task");
const user = require("../../models/user");

module.exports = async (req, res) => {
  try {
    console.log(req.body);
    const { content, idTask } = req.body;
    const userEmail = req.user;
    if (!userEmail) {
      return res.status(401).send({
        type: 'error',
        message: 'Realizar o login novamente'
      });
    }
    const userExists = await user.findOne({
      where: {
        email:userEmail.email
      }
    });

    const idUser = userExists.id;
    console.log({idUser});
    if(!idUser) {
      return res.status(401).send({
        type: 'error',
        message: 'Usuário não encontrado'
      });
    }
    console.log({content, idUser, idTask});
    if (!content || !idUser || !idTask) {
      console.log('chegou aqui');
      return res.status(400).send({
        type: 'error',
        message: 'É necessario informar todos os campos para adicionar o registro'
      });
    }
    console.log({content: typeof content, idUser: typeof idUser, idTask: typeof idTask});
    if (typeof content === 'string' && typeof idUser === 'number' && typeof idTask === 'number') {

      const taskExists = await task.findOne({ where: { id: idTask } });
      const userExists = await user.findOne({ where: { id: idUser } });

      if (taskExists && taskExists.id && userExists && userExists.id) {
        let response = await comment.create({ content, idUser, idTask });

        if (typeof response.id === 'number') {
          return res.status(201).send({
            type: 'success',
            message: 'Registros inseridos com sucesso',
          });
        } else {
          console.log(response);
          return res.status(500).send({
            type: 'error',
            message: 'Não foi possivel inserir o novo registro',
          });
        }
      } else {
        return res.status(409).send({
          type: 'error',
          message: 'Não foi possivel inserir o novo registro',
        });
      }
    } else {
      return res.status(400).send({
        type: 'error',
        message: 'Dados invalidos.',
      });
    }

  } catch (error) {
    console.log(error);
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
};
