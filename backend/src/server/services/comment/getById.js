const comment = require("../../models/comment");

module.exports =  async (req, res) => {
  try {
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    
    if (id && typeof id === 'number') {
      const commentExists = await comment.findOne({ where: { id } });
      
      if (typeof commentExists.id === 'number') {
        return res.status(200).send({
          type: 'success',
          data: commentExists
        })
      }
      
    }
    return res.status(400).send({
      type: 'error',
      message: 'Registro não encontrado.'
    }); 
  } catch (error) {
    return res.status(400).send({
      type: 'error',
      message: error.message
    });
  }
}
