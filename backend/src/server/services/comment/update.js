const comment = require("../../models/comment");

module.exports =  async (req, res) => {
  try {
    const { content, idUser, idTask } = req.body;
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    
    if (!id || !content || !idUser || !idTask) {
      return res.status(400).send({
        type: 'error',
        message: `É necessario informar todos o dados do registro`
      });
    }

    if (typeof id === 'number' && typeof content === 'string' && typeof idUser === 'number' && typeof idTask === 'number') {

      const commentExists = await comment.findOne({ where: { id } });
      
      if(!!commentExists) {
        let response = await commentExists.update({ content, idUser, idTask });
        return res.status(201).send({
            type: 'success',
            message: 'Registro atualizado com sucesso',
            data: response
        });
      } else {
        return res.status(400).send({
          type: 'error',
          message: 'Não foi possivel atualizar os dados.',
        });
      }
    } else {
      return res.status(404).send({
        type: 'error',
        message: 'Não foi possivel atualizar os dados.',
      });
    }

  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
