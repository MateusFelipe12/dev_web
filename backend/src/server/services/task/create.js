const task = require("../../models/task");

module.exports =  async (req, res) => {
  try {
    const { status, description, title } = req.body;
    console.log(req.body);
    
    if (!status || !description || !title) {
      return res.status(400).send({
        type: 'error',
        message: `É necessario informar todos os campos para adicionar o registro`
      });
    }

    if (typeof status === 'string' && typeof description === 'string' && typeof title === 'string') {


      let response = await task.create({ status, description, title });
      
      if (typeof response.id === 'number') {
        return res.status(201).send({
            type: 'success',
            message: 'Registros inseridos com sucesso',
            data: response
        });
      } else {
        return res.status(500).send({
          type: 'error',
          message: 'Não foi possivel inserir o novo registro',
        });
      }
    } else {
      return res.status(400).send({
        type: 'error',
        message: 'Dados invalidos.',
      });
    }

  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
