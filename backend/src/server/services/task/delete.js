const task = require("../../models/task");
const comment = require("../../models/comment");
const tag = require("../../models/tag");

module.exports =  async (req, res) => {
  try {
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    
    if (id && typeof id === 'number') {
      const taskExists = await task.findOne({ where: { id } });
      
      if(!!taskExists) {
        await comment.destroy({ where: { idTask: id } });
        await tag.destroy({ where: { idTask: id } });
        let response = await taskExists.destroy();
        if ( response !== null ) {
          return res.status(204).send();
        }
      }
    }
    return res.status(400).send({
      type: 'error',
      message: 'Não foi possivel deletar o registro.',
    });
  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
