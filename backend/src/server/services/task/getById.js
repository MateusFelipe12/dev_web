const task = require("../../models/task");

module.exports =  async (req, res) => {
  try {
    const {id} = req.params;

    const data = await task.findOne({where: {id}});
    return res.status(200).send({
      type: 'success',
      data: data
    })
  } catch (error) {
    return res.status(400).send({
      type: 'error',
      message: error.message
    });
  }
}
