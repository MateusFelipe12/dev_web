const task = require("../../models/task");

module.exports =  async (req, res) => {
  try {
    console.log(req.body);
    const { status, description, title } = req.body;
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    console.log({ status, description, title, id });
    
    if (!id || !status || !description || !title) {
      return res.status(400).send({
        type: 'error',
        message: `É necessario informar todos o dados do registro`
      });
    }

    if (typeof id === 'number' && typeof status === 'string' && typeof description === 'string' && typeof title === 'string') {

      const taskExists = await task.findOne({ where: { id } });
      
      if(!!taskExists) {
        let response = await taskExists.update({ status, description, title });
        return res.status(201).send({
            type: 'success',
            message: 'Registro atualizado com sucesso',
            data: response
        });
      } else {
        return res.status(400).send({
          type: 'error',
          message: 'Não foi possivel atualizar os dados.',
        });
      }
    } else {
      return res.status(404).send({
        type: 'error',
        message: 'Não foi possivel atualizar os dados.',
      });
    }

  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
