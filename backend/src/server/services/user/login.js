const User = require("../../models/user");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports =  async ({ email, password }, res) => {
    try {
        const user = await User.findOne({
            where: {
                email
            }
        });
  
        console.log('chegou aqui1')
        if (!user) {
            console.log('chegou aqui2')
            return res.send(
                { success: false, error: 'User not found' }
            );
        }
        console.log('chegou aqui3')
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            return res.send(
                { success: false, error: 'Invalid credentials' }
            );
        }
        console.log('chegou aqui4')
        const token = jwt.sign({ email: user.email }, process.env.JWT_SECRET, {
            expiresIn: '1h',
        });
        
        console.log('TOKEN', {token})
        return res.send({
            success: true,
            data: {
                token,
            }
        });
    } catch (error) {
        console.log(error)
        return res.send({ success: false, error: 'Internal server error' });
    }
  }