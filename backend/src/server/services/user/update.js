const user = require("../../models/user");

module.exports =  async (req, res) => {
  try {
    const { name, password } = req.body;
    const email = req.params.email;
    
    if (!name || !email) {
      return res.status(400).send({
        type: 'error',
        message: `É necessario informar todos o dados do registro`
      });
    }
    
    if (typeof name === 'string' && typeof email === 'string' && typeof password === 'string') {

      const userExists = await user.findOne({ where: { email } });
      
      if(!!userExists) {
        await userExists.update({ name, password });
        let response = await user.findOne({
            attributes: ['name', 'email']
        });
        
        return res.status(201).send({
            type: 'success',
            message: 'Registro atualizado com sucesso',
            data: response
        });
      } else {
        return res.status(400).send({
          type: 'error',
          message: 'Não foi possivel atualizar os dados.',
        });
      }
    } else {
      return res.status(404).send({
        type: 'error',
        message: 'Não foi possivel atualizar os dados.',
      });
    }

  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
