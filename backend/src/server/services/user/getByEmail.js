const user = require("../../models/user");

module.exports =  async (req, res) => {
  try {
    const {email} = req.params;
    console.log(email)

    const data = await user.findOne({
      where: {email},
      attributes: ['name', 'email']
    });
    return res.status(200).send({
      type: 'success',
      data: data
    })
  } catch (error) {
    return res.status(400).send({
      type: 'error',
      message: error.message
    });
  }
}
