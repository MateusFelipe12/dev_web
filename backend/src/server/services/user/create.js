const user = require("../../models/user");
const bcrypt = require('bcrypt');

module.exports =  async (req, res) => {
  try {
    const { name, email, password } = req.body;

    if (!name || !email || !password) {
      return res.status(400).send({
        type: 'error',
        message: `É necessario informar todos os campos para adicionar o registro`
      });
    }
    
    if (typeof name === 'string' && typeof email === 'string' && typeof password === 'string') {

      const userExists = await user.findOne({ where: { email } });
      
      if(!userExists) {
        const saltRounds = parseInt(process.env.SALT_ROUNDS_PASSWORD) || 10;
        const hashedPassword = await bcrypt.hash(password, saltRounds);

        let response = await user.create({ name, email, password: hashedPassword });
        
        if (typeof response.id === 'number') {
          return res.status(201).send({
              type: 'success',
              message: 'Registros inseridos com sucesso',
              data: response.id
          });
        } else {
          return res.status(500).send({
            type: 'error',
            message: 'Não foi possivel inserir o novo registro',
          });
        }
      } else {
        return res.status(409).send({
          type: 'error',
          message: 'Não foi possivel inserir o novo registro',
        });
      }
    } else {
      return res.status(400).send({
        type: 'error',
        message: 'Dados invalidos.',
      });
    }

  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
