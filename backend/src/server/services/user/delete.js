const user = require("../../models/user");

module.exports =  async (req, res) => {
  try {
    const idCrude = Number(req.params.id);
    const id  = idCrude !== NaN ? idCrude : false;
    
    if (id && typeof id === 'number') {
      const userExists = await user.findOne({ where: { id } });
      
      if(!!userExists) {
        let response = await userExists.destroy();
        if ( response !== null ) {
          return res.status(204).send();
        }
      }
    }
    return res.status(400).send({
      type: 'error',
      message: 'Não foi possivel deletar o registro.',
    });
  } catch (error) {
    return res.status(500).send({
      type: 'error',
      message: error.message
    });
  }
}
