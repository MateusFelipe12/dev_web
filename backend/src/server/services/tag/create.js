const Tag = require("../../models/tag");
const task = require("../../models/task");

module.exports =  async (req, res) => {
  try {
    let { name, color, idTask} = req.body;
    console.log(req.body);
    if (!name || !color) {
      return res.send({
        type: 'error',
        message: `É necessario informar todos os campos para adicionar o registro`
      });
    }

    if (typeof color === 'string' && typeof name === 'string' && typeof idTask === 'number') {

        const tagExists = await Tag.findOne({ where: { name } });
        const taskExists = await task.findOne({ where: { id: idTask } });

        if (!tagExists && taskExists) {
            let response = await Tag.create({ name, color, idTask });

            return res.status(200).send({
                type: 'success',
                message: 'Registro criado com sucesso',
                data: response
            });
        } else {
            return res.status(400).send({
                type: 'error',
                message: 'Erro ao criar registro, projeto já existe'
            });
        }
    } else {
        return res.status(400).send({
            type: 'error',
            message: 'Erro ao criar registro, tipo de dado inválido'
        });
    }
  } catch (error) {
    return res.send({
      type: 'error',
      message: error.message
    });
  }
}
