const Tag = require("../../models/tag");

module.exports = async (req, res) => {
    try {
        let { id } = req.params;
        const { name, color } = req.body;
        const response = await Tag.update({ name, color }, {
            where: {
                id: id
            }
        });

        if (!response) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado o registro!',
            data: []
            });
        }
    
        return res.status(200).send({
            type: 'success',
            message: 'Registro atualizado com sucesso!',
            data: response
        });    
    } catch (error) {
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao atualizar o registro!',
            data: []
        });
    }
}