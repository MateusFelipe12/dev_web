const Tag = require("../../models/tag");

module.exports = async (req, res) => {
    try {
        let { idTask } = req.params;

        const response = await Tag.findAll({
            where: {
                idTask
            }
        });
        if (!response.length) {
            return res.status(200).send({
            type: 'warning',
            message: 'Não foi encontrado os registro!',
            data: []
            });
        }
        return res.status(200).send({
            type: 'success',
            message: 'Registros encontrados com sucesso!',
            data: response
        });
    } catch (error) {
        console.log(error);
        return res.status(500).send({
            type: 'error',
            message: 'Erro ao buscar os registros!',
            data: []
        });
    }
}