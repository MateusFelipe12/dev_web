const getTaskService = require('../../services/task/get');

module.exports = async (req, res) => {
  return getTaskService(req, res);
}