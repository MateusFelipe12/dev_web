const createTaskService = require('../../services/task/create');

module.exports = async (req, res) => {
  return createTaskService(req, res);
}