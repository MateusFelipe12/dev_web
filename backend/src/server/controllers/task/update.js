const updateTaskService = require('../../services/task/update');

module.exports = async (req, res) => {
  return updateTaskService(req, res);
}