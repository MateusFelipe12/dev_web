const deleteTaskService = require('../../services/task/delete');

module.exports = async (req, res) => {
  return deleteTaskService(req, res);
}