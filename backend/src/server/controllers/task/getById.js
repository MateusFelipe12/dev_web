const getByIdTaskService = require('../../services/task/getById');

module.exports = async (req, res) => {
  return getByIdTaskService(req, res);
}