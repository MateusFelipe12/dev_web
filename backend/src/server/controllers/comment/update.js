const updateCommentService = require('../../services/comment/update');

module.exports = async (req, res) => {
  return updateCommentService(req, res);
}