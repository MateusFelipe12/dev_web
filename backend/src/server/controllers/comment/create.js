const createCommentService = require('../../services/comment/create');

module.exports = async (req, res) => {
  return createCommentService(req, res);
}