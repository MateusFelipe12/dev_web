const getByIdCommentService = require('../../services/comment/getById');

module.exports = async (req, res) => {
  return getByIdCommentService(req, res);
}