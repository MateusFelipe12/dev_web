const getByTaskCommentService = require('../../services/comment/getByTask');

module.exports = async (req, res) => {
  return getByTaskCommentService(req, res);
}