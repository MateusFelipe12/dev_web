const getCommentService = require('../../services/comment/get');

module.exports = async (req, res) => {
  return getCommentService(req, res);
}