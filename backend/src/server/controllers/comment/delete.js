const deleteCommentService = require('../../services/comment/delete');

module.exports = async (req, res) => {
  return deleteCommentService(req, res);
}