const createTagService = require('../../services/tag/create');

module.exports = async (req, res) => {
  return createTagService(req, res);
} 