const deleteTagService = require('../../services/tag/delete');

module.exports = async (req, res) => {

  return deleteTagService(req, res);
}