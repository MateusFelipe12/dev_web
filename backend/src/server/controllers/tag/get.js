const getTagService = require('../../services/tag/get');

module.exports = async (req, res) => {
  
  return getTagService(req, res);
}