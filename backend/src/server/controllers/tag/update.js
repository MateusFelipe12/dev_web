const updateTagService = require('../../services/tag/update');

module.exports = async (req, res) => {

  return updateTagService(req, res);
}