const getByTaskTagService = require('../../services/tag/getByTask');

module.exports = async (req, res) => {
  return getByTaskTagService(req, res);
}