const getUserService = require('../../services/user/get');

module.exports = async (req, res) => {
  return getUserService(req, res);
}