const getByEmailUserService = require('../../services/user/getByEmail');

module.exports = async (req, res) => {
  return getByEmailUserService(req, res);
}