const loginUserService = require('../../services/user/login');

module.exports = async (req, res) => {
  const { email, password } = req.body;
  return await loginUserService({ email, password }, res);
}