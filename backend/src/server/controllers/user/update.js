const updateUserService = require('../../services/user/update');

module.exports = async (req, res) => {
  return updateUserService(req, res);
}