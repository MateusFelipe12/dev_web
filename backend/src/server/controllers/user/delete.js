const deleteUserService = require('../../services/user/delete');

module.exports = async (req, res) => {
  return deleteUserService(req, res);
}