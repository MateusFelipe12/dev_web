const createUserService = require('../../services/user/create');

module.exports = async (req, res) => {
  return createUserService(req, res);
}