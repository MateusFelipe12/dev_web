const getByIdUserService = require('../../services/user/getById');

module.exports = async (req, res) => {
  return getByIdUserService(req, res);
}