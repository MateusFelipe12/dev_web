const express = require('express');
require('dotenv').config();
require('./models');
const cors = require('cors');

const Routes = require('./routes');

const server = express();

const corsOptions = {
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization']
};

server.use(cors(corsOptions));

server.use(express.json());

Routes(server);

module.exports = server;