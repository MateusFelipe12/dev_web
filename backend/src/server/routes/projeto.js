const fs = require('fs');
const { checkTokenMiddleware } = require('../utils/middleware');

const projetoController = {};

// Lê todos os arquivos na pasta 'controllers/projeto'
const projetoPath = 'src/server/controllers/projeto';
fs.readdirSync(projetoPath).forEach(file => {
	const method = file.replace('.js', '');
	projetoController[method] = require('../controllers/projeto/' + file);
});

module.exports = (app) => {
	// }));
    app.get('/projeto', checkTokenMiddleware, projetoController.get);
    app.get('/projeto/:id', checkTokenMiddleware, projetoController.getById);
    app.post('/projeto', checkTokenMiddleware, projetoController.create);
    app.put('/projeto/:id', checkTokenMiddleware, projetoController.update);
    app.delete('/projeto', checkTokenMiddleware, projetoController.delete);
};
