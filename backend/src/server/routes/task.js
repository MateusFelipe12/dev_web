const fs = require('fs');
const { checkTokenMiddleware } = require('../utils/middleware');

const taskController = {};

// Lê todos os arquivos na pasta 'controllers/items'
const itemsPath = 'src/server/controllers/task';
fs.readdirSync(itemsPath).forEach(file => {
	const method = file.replace('.js', '');
	taskController[method] = require('../controllers/task/' + file);
});

module.exports = (app) => {
    app.post('/task', checkTokenMiddleware, taskController.create);
    app.get('/task', checkTokenMiddleware, taskController.get);
    app.get('/task/:id', checkTokenMiddleware, taskController.getById);
    app.put('/task/:id', checkTokenMiddleware, taskController.update);
    app.delete('/task/:id', checkTokenMiddleware, taskController.delete);
};
