const projeto = require('./projeto');
const user = require('./user');
const comment = require('./comment');
const task = require('./task');
const tag = require('./tag')
require('dotenv').config();
const bodyParser = require('body-parser');

module.exports = function Routes(app) {
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());

	projeto(app);
	user(app);
	comment(app);
	task(app);
	tag(app);
}
