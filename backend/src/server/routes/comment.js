const fs = require('fs');
const { checkTokenMiddleware } = require('../utils/middleware');

const commentController = {};

// Lê todos os arquivos na pasta 'controllers/comment'
const itemsPath = 'src/server/controllers/comment';
fs.readdirSync(itemsPath).forEach(file => {
	const method = file.replace('.js', '');
	commentController[method] = require('../controllers/comment/' + file);
});

module.exports = (app) => {
    app.post('/comment', checkTokenMiddleware, commentController.create);
    app.get('/comment', checkTokenMiddleware, commentController.get);
    app.get('/comment/:id', checkTokenMiddleware, commentController.getById);
    app.get('/comment/task/:idTask', checkTokenMiddleware, commentController.getByTask);
    app.put('/comment/:id', checkTokenMiddleware, commentController.update);
    app.delete('/comment/:id', checkTokenMiddleware, commentController.delete);
};
