const fs = require('fs');
const { checkTokenMiddleware } = require('../utils/middleware');

const tagController = {};

// Lê todos os arquivos na pasta 'controllers/projeto'
const tagPath = 'src/server/controllers/tag';
fs.readdirSync(tagPath).forEach(file => {
	const method = file.replace('.js', '');
	tagController[method] = require('../controllers/tag/' + file);
});

module.exports = (app) => {
    app.get('/tag', checkTokenMiddleware, tagController.get);
    app.get('/tag/:id', checkTokenMiddleware, tagController.getById);
    app.get('/tag/task/:idTask', checkTokenMiddleware, tagController.getByTask);
    app.post('/tag', checkTokenMiddleware, tagController.create);
    app.put('/tag/:id', checkTokenMiddleware, tagController.update);
    app.delete('/tag', checkTokenMiddleware, tagController.delete);
};
