const fs = require('fs');
const { checkTokenMiddleware } = require('../utils/middleware');

const userController = {};

// Lê todos os arquivos na pasta 'controllers/items'
const itemsPath = 'src/server/controllers/user';
fs.readdirSync(itemsPath).forEach(file => {
	const method = file.replace('.js', '');
	userController[method] = require('../controllers/user/' + file);
});

module.exports = (app) => {
    app.post('/login', userController.login);
    app.post('/user', userController.create);
    app.get('/user', checkTokenMiddleware, userController.get);
    app.get('/user/:id', checkTokenMiddleware, userController.getById);
    app.get('/user/email/:email', checkTokenMiddleware, userController.getByEmail);
    app.put('/user/:email', checkTokenMiddleware, userController.update);
    app.delete('/user/:id', checkTokenMiddleware, userController.delete);
};
