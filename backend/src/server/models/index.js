const user = require('./user');
const task = require('./task');
const tag = require('./tag');
const projeto = require('./projeto');
const comment = require('./comment');

  

(async () => {
  await projeto.sync();
  await user.sync();
  await task.sync();
  await comment.sync();
  await tag.sync();
})();