const { DataTypes } = require("sequelize");
const sequelize =  require("../config/database");
const Task = require("./task");

const tag = sequelize.define(
  'tag',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    color: {
      type: DataTypes.STRING,
      allowNull: false
    },
    idTask: {
      type: DataTypes.INTEGER,
      references: {
        model: Task,
        key: 'id',
      },
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
);

module.exports = tag;