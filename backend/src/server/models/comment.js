const { DataTypes } = require("sequelize");
const sequelize = require("../config/database");
const Task = require("./task");
const User = require("./user");

const Comment = sequelize.define(
  'comment',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    idTask: {
      type: DataTypes.INTEGER,
      references: {
        model: Task,
        key: 'id',
      },
      allowNull: false,
    },
    idUser: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: 'id',
      },
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
);

// Definindo as associações
Comment.belongsTo(Task, { foreignKey: 'idTask' });
Comment.belongsTo(User, { foreignKey: 'idUser' });

module.exports = Comment;
