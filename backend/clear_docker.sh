echo "Removendo todos os containers, imagens, networks e volumes do docker..."
echo "START"
docker stop $(docker ps -aq)
docker rm $(docker ps -a -q)
docker rm $(docker ps -aq)
docker image prune
docker rmi $(docker images -a -q)
docker network prune
docker network rm $(docker network ls -q)
docker volume prune
docker volume rm $(docker volume ls -q)
docker image prune -a
docker container prune -a
